//
//  JTSpotifyPlaylistHelper.h
//  Jarre
//
//  Created by Kong king sin on 18/9/14.
//  Copyright (c) 2014 Kong king sin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JTSpotifyPlaylistHelper : NSObject
+ (void)fetchAllUserPlaylistsWithSession:(SPTSession *)session callback:(void (^)(NSError *, NSArray *))callback;
+ (void)didFetchListPageForSession:(SPTSession *)session finalCallback:(void (^)(NSError*, NSArray*))finalCallback error:(NSError *)error object:(id)object allPlaylists:(NSMutableArray *)allPlaylists;
@end
