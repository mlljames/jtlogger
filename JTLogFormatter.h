//
//  EHLogFormatter.h
//  GeoMonitoringTest
//
//  Created by Jack Lee on 12/4/24.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDLog.h"

@interface JTLogFormatter : NSObject <DDLogFormatter>

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end
