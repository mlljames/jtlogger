//
//  Helper.m
//  Jarre
//
//  Created by Kong king sin on 9/10/14.
//  Copyright (c) 2014 Kong king sin. All rights reserved.
//

#import "Helper.h"
#include <CommonCrypto/CommonCryptor.h>

#import <CommonCrypto/CommonDigest.h>

@implementation Helper

+ (CGRect)innerBoundsFromViewController:(UIViewController*)vc
{
    CGRect bounds = [[UIScreen mainScreen] bounds];
    if (vc.tabBarController) {
        bounds.size.height -= vc.tabBarController.tabBar.bounds.size.height;
    }
    if (vc.navigationController && !vc.navigationController.isNavigationBarHidden) {
        bounds.size.height -= vc.navigationController.navigationBar.bounds.size.height;
    }
    if (![[UIApplication sharedApplication] isStatusBarHidden]) {
        bounds.size.height -= 20;
    }
    return bounds;
}

+ (void)addMinMaxParamToDict:(NSMutableDictionary *)dictionary str:(NSString *)minmaxStr
{
    NSArray *minmax = [minmaxStr componentsSeparatedByString:@"-"];
    NSString *min = [minmax objectAtIndex:0];
    NSString *max = [minmax objectAtIndex:1];
    
    [dictionary setObject:min forKey:@"minpoint"];
    [dictionary setObject:max forKey:@"maxpoint"];
}

+ (NSDictionary*)queryComponents:(NSURL *)url
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [url.query componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        NSString *value = [self stringByDecodingURLFormat:[elts objectAtIndex:1]];
        NSString *key = [self stringByDecodingURLFormat:[elts objectAtIndex:0]];
        [params setObject:value forKey:key];
    }
    return params;
}

+ (NSString*)addQueryStringToUrlString:(NSString *)urlString withDictionary:(NSDictionary *)dictionary
{
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:urlString];
    
    for (id key in dictionary) {
        NSString *keyString = [key description];
        NSString *valueString = [[dictionary objectForKey:key] description];
        
        if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
            [urlWithQuerystring appendFormat:@"?%@=%@", [self stringByEncodingURLFormat:keyString], [self stringByEncodingURLFormat:valueString]];
        } else {
            [urlWithQuerystring appendFormat:@"&%@=%@", [self stringByEncodingURLFormat:keyString], [self stringByEncodingURLFormat:valueString]];
        }
    }
    return urlWithQuerystring;
}

+ (NSString *)stringByDecodingURLFormat:(NSString*)str
{
    NSString *result = [str stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

+ (NSString *)stringByEncodingURLFormat:(NSString*)str
{
    NSString *result = [str stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    result = [result stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}


+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

+ (NSString*)rc4:(NSString*)str key:(NSString*)key
{
    int j = 0;
    unichar res[str.length];
    const unichar* buffer = res;
    unsigned char s[256];
    for (int i = 0; i < 256; i++)
    {
        s[i] = i;
    }
    for (int i = 0; i < 256; i++)
    {
        j = (j + s[i] + [key characterAtIndex:(i % key.length)]) % 256;
        
        unsigned char tempVar; // make a temporary variable
        tempVar = s[i];
        s[i] = s[j];
        s[j] = tempVar;
        //std::swap(s[i], s[j]);
    }
    
    int i = j = 0;
    
    for (int y = 0; y < str.length; y++)
    {
        i = (i + 1) % 256;
        j = (j + s[i]) % 256;
        
        unsigned char tempVar; // make a temporary variable
        tempVar = s[i];
        s[i] = s[j];
        s[j] = tempVar;
        //std::swap(s[i], s[j]);
        
        unsigned char f = [str characterAtIndex:y] ^ s[ (s[i] + s[j]) % 256];
        res[y] = f;
    }
    return [NSString stringWithCharacters:buffer length:str.length];
}

+ (void)showLoading
{
    //    [self hideLoading];
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.color = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
    //    hud.opacity = 0.9;
    //    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading0001.png"]];
    //    iv.frame = CGRectMake(0, 0, 150, 150);
    //    NSMutableArray *images = [NSMutableArray array];
    //    for (int i=1; i <= 22; i++) {
    //        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loading%.04d.png", i]]];
    //    }
    //    iv.animationImages = images;
    //    iv.animationDuration = 1.0f;
    //    [iv startAnimating];
    //    hud.customView = iv;
    //    hud.dimBackground = YES;
}

+ (void)hideLoading
{
    //    [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
}

+ (void)recordViewedTask:(NSInteger)newID
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *taskIDs = [NSMutableArray arrayWithArray:[userDefaults arrayForKey:@"tasks"]];
    BOOL isFound = FALSE;
    for (NSNumber *taskID in taskIDs) {
        if ([taskID intValue] == newID) {
            isFound = TRUE;
        }
    }
    
    if (!isFound) {
        [taskIDs addObject:[NSNumber numberWithInteger:newID]];
    }
    [userDefaults setObject:taskIDs forKey:@"tasks"];
    [userDefaults synchronize];
}

+ (NSArray*)viewedTasks
{
    return [[NSUserDefaults standardUserDefaults] arrayForKey:@"tasks"];
}

+ (BOOL)isValidTextInput:(NSString *)textString
{
    NSString *regExPattern = @"[^a-zA-Z0-9]+";
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:textString options:0 range:NSMakeRange(0, [textString length])];
    
    return match == 0;
}

+ (BOOL)isValidEmail:(NSString *)emailString
{
    NSString *regExPattern = @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    return match != 0;
}

+ (BOOL)isValidPassword:(NSString *)passwordString
{
    if (passwordString.length >= 8 &&
        (![[passwordString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]] isEqualToString:passwordString] &&
         ![[passwordString stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]] isEqualToString:passwordString])) {
            return YES;
        }
    return NO;
}

+ (BOOL)isNonEmptyText:(NSString *)text
{
    if (!text) {
        return NO;
    }
    
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    return ![text isEqualToString:@""];
}

//+ (UIAlertView *)showConfirmAlertBoxWithTitleKey:(NSString *)titleKey messageKey:(NSString *)messageKey
//{
//    return [Helper showConfirmAlertBoxWithTitle:nil != titleKey? [[LangMgr sharedInstance] getStringWithKey:titleKey] : nil
//                                        message:nil != messageKey? [[LangMgr sharedInstance] getStringWithKey:messageKey] : nil];
//}

//+ (UIAlertView *)showConfirmAlertBoxWithTitleKey:(NSString *)titleKey messageKey:(NSString *)messageKey delegate:(id<UIAlertViewDelegate>)delegate
//{
//    return [Helper showConfirmAlertBoxWithTitle:nil != titleKey? [[LangMgr sharedInstance] getStringWithKey:titleKey] : nil
//                                        message:nil != messageKey? [[LangMgr sharedInstance] getStringWithKey:messageKey] : nil
//                                       delegate:delegate];
//}

+ (UIAlertView *)showConfirmAlertBoxWithTitle:(NSString *)title message:(NSString *)message
{
    return [Helper showConfirmAlertBoxWithTitle:title message:message delegate:nil];
}

//+ (UIAlertView *)showConfirmAlertBoxWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate
//{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:delegate
//                                              cancelButtonTitle:[[LangMgr sharedInstance] getStringWithKey:@"register_login_confirm"]
//                                              otherButtonTitles:nil];
//    [alertView show];
//    return alertView;
//}


+ (void)styleLoginTextField:(UITextField *)textField
{
    // add Left Padding
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 34, 1)];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = spacerView;
    
    [textField setValue:[UIColor colorWithWhite:0.75 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
}

+ (NSString *)directoryPathOfAppFiles
{
    
    NSString *bundleID = [[NSBundle mainBundle] bundleIdentifier];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *dirPath = nil;
    
    // Find the application support directory in the home directory.
    NSArray* appSupportDir = [fileManager URLsForDirectory:NSApplicationSupportDirectory
                                                 inDomains:NSUserDomainMask];
    if ([appSupportDir count] > 0)
    {
        // Append the bundle ID to the URL for the
        // Application Support directory
        dirPath = [appSupportDir[0] URLByAppendingPathComponent:bundleID];
        
        // If the directory does not exist, this method creates it.
        NSError* theError = nil;
        if (![fileManager createDirectoryAtURL:dirPath withIntermediateDirectories:YES attributes:nil error:&theError]) {
            // Handle the error.
            return @"";
        }
        
        [Helper addSkipBackupAttributeToItemAtURL:dirPath];
    }
    
    return [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:bundleID];
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        DDLogVerbose(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


+(NSString *) utf8ToUnicode:(NSString *)string

{
    
    NSUInteger length = [string length];
    
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    
    for (int i = 0;i < length; i++)
        
    {
        
        unichar _char = [string characterAtIndex:i];
        
        //判断是否为英文和数字
        if ((_char >= ' ' && _char <= '~') ||  _char == '\n'){
            [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i,1)]];
        }else{
            [s appendFormat:@"\\u%x",[string characterAtIndex:i]];
        }
        /*
         if (_char <= '9' && _char >='0')
         
         {
         
         [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i,1)]];
         
         }
         
         else if(_char >='a' && _char <= 'z')
         
         {
         
         [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i,1)]];
         
         
         
         }
         
         else if(_char >='A' && _char <= 'Z')
         
         {
         
         [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i,1)]];
         
         
         
         }
         else if(_char =='{' || _char == '}' || _char == ',' || _char == '\n' || _char == '[' || _char == ']' || _char == ':' || _char == '"' || _char == '.' || _char == ' '|| _char == '_' || _char == '|')
         
         {
         
         [s appendFormat:@"%@",[string substringWithRange:NSMakeRange(i,1)]];
         
         
         
         }
         
         else
         
         {
         
         [s appendFormat:@"\\u%x",[string characterAtIndex:i]];
         
         }
         */
    }
    
    return s;
    
}
@end