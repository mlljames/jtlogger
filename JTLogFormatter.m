//
//  EHLogFormatter.m
//  GeoMonitoringTest
//
//  Created by Jack Lee on 12/4/24.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "JTLogFormatter.h"

@implementation JTLogFormatter

@synthesize dateFormatter = _dateFormatter;

- (id)init
{
    if((self = [super init]))
    {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [_dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss:SSS"];
    }
    return self;
}

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage
{
    NSString *logLevel;
    switch (logMessage->logFlag)
    {
        case LOG_FLAG_ERROR : {
           logLevel = @"ERROR"; break; 
        }
        case LOG_FLAG_WARN  : {
            logLevel = @"WARN"; break;
        }
        case LOG_FLAG_INFO  : {
            logLevel = @"INFO"; break;
        }
        case LOG_FLAG_LIFECYCLE  :
        case LOG_FLAG_LIB_LIFECYCLE  : {
            logLevel = @"LIFECYCLE"; break;
        }
        case LOG_FLAG_PACE  :
        case LOG_FLAG_LIB_PACE  : {
            logLevel = @"PACE"; break;
        }
        case LOG_FLAG_VERBOSE  : 
        case LOG_FLAG_LIB_VERBOSE  : {
            logLevel = @"VERBOSE"; break;
        }
        default             : {
           logLevel = @"DEBUG"; break; 
        }
    }
    
//    NSString *dateAndTime = [self.dateFormatter stringFromDate:(logMessage->timestamp)];
    NSString *logMsg = logMessage->logMsg;
    return [NSString stringWithFormat:@"[%@] %@", logLevel, logMsg];
//    return [NSString stringWithFormat:@"%@ [%@] %@", dateAndTime, logLevel, logMsg];
}

@end
