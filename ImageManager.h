//
//  ImageManager.h
//  Jarre
//
//  Created by Kong king sin on 9/10/14.
//  Copyright (c) 2014 Kong king sin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageManager : NSObject

+ (id)sharedInstance;
- (void) loadImageToImageview:(AsyncImageView*)asyncImageView_
                    imagePath:(NSString*) path_
                     loadFrom:(int) loadFrom_;
- (UIImage*) loadAImageWithPath:(NSString*) path_
                       loadFrom:(int) loadFrom_;
- (void)downloadImageWithURL:(NSString *)url_
             completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock_;

@end
