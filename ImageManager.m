//
//  ImageManager.m
//  Jarre
//
//  Created by Kong king sin on 9/10/14.
//  Copyright (c) 2014 Kong king sin. All rights reserved.
//

#import "ImageManager.h"
#import "Helper.h"
@implementation ImageManager
+ (id)sharedInstance
{
    DDLogPace(@"%s", __PRETTY_FUNCTION__);
    static ImageManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
//- (void) loadImageToImageview:(AsyncImageView*) asyncImageView_ imagePath:(NSString*) path_ loadFrom:(int) loadFrom_
//{
//    switch (loadFrom_) {
//        case LOAD_FROM_APP_RESOURCE:
//            asyncImageView_.image = [UIImage imageNamed:path_];
//            break;
//        case LOAD_FROM_SERVER:
//            asyncImageView_.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASE_URL_CDN, BASE_RESOURCE_URL, path_]];
//            break;
//        case LOAD_FROM_ZIP:
//            asyncImageView_.image = [[LangMgr sharedInstance] getImageWithKey:path_];
//            break;
//            
//        default:
//            break;
//    }
//    
//}
//
//
//- (UIImage*) loadAImageWithPath:(NSString*) path_ loadFrom:(int) loadFrom_
//{
//    UIImage* imgReturn = nil;
//    switch (loadFrom_) {
//        case LOAD_FROM_APP_RESOURCE:
//            imgReturn = [UIImage imageNamed:path_];
//            break;
//            //        case LOAD_FROM_SERVER:
//            //            asyncImageView_.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASE_URL, BASE_RESOURCE_URL, path_]];
//            //            break;
//        case LOAD_FROM_ZIP:
//            imgReturn = [[LangMgr sharedInstance] getImageWithKey:path_];
//            break;
//            
//        default:
//            break;
//    }
//    
//    return imgReturn;
//    
//}

- (void)downloadImageWithURL:(NSString *)url_ completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock_
{
    DDLogPace(@"%s", __PRETTY_FUNCTION__);
    
    if([self checkFileExistName:url_]){
        // exist , get back cache
        DDLogVerbose(@"fileExisted");
        completionBlock_(true, [self getSaveImageName:url_]);
    }else{
        // not exist , download and cache it
        DDLogVerbose(@"going to download...");
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSURL* url = [NSURL URLWithString:url_];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                completionBlock_(true, image);
                // save image to cahed directory
                [self saveImageName:url_ image:image];
            }else{
                completionBlock_(false, nil);
            }
            
        });
    }
}
#pragma mark - Cache image
- (void) saveImageName:(NSString*)imageName_ image:(UIImage*) image_
{
    NSError *error = nil;
    NSString *path = [Helper directoryPathOfAppFiles];
    NSString *cacheImageFolderPath = [path stringByAppendingPathComponent:@"local_cache_image_folder"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:cacheImageFolderPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheImageFolderPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    NSString* imagePath = [NSString stringWithFormat:@"%@/%@",cacheImageFolderPath, [self makeCacheNameFrom:imageName_]];
    NSData *imageData = nil;
    
    
    if ([imageName_ rangeOfString:@".png"].location == NSNotFound) {
        imageData = UIImagePNGRepresentation(image_);
        //        imageData = UIImageJPEGRepresentation(image_, 0.7);
    }else{
        //        imageData = UIImageJPEGRepresentation(image_, 0.7);
        imageData = UIImagePNGRepresentation(image_);
    }
    [imageData writeToFile:imagePath options:0 error:&error];
    // done save
}

- (UIImage*) getSaveImageName:(NSString*)imageName_
{
    
    NSString *path = [Helper directoryPathOfAppFiles];
    NSString *cacheImageFolderPath = [path stringByAppendingPathComponent:@"local_cache_image_folder"];
    NSString* imagePath = [NSString stringWithFormat:@"%@/%@",cacheImageFolderPath, [self makeCacheNameFrom:imageName_]];
    NSData* imageData = [NSData dataWithContentsOfFile:imagePath options:0 error:nil];
    return [UIImage imageWithData:imageData];
}

- (NSString*) makeCacheNameFrom:(NSString*) sourceStr_
{
    NSString *str =[sourceStr_ stringByReplacingOccurrencesOfString:kBaseURL withString:@""];
//    str =[str stringByReplacingOccurrencesOfString:BASE_URL_CDN withString:@""];
    return  [str stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
}

- (bool) checkFileExistName:(NSString*) fileName_
{
    NSString *path = [Helper directoryPathOfAppFiles];
    NSString *cacheImageFolderPath = [path stringByAppendingPathComponent:@"local_cache_image_folder"];
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",cacheImageFolderPath, [self makeCacheNameFrom:fileName_]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        return true;
    }else{
        return false;
    }
}
@end
