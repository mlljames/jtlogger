//
//  Helper.h
//  Jarre
//
//  Created by Kong king sin on 9/10/14.
//  Copyright (c) 2014 Kong king sin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+Base64.h"

@interface Helper : NSObject

+ (CGRect)innerBoundsFromViewController:(UIViewController*)vc;
+ (void)addMinMaxParamToDict:(NSMutableDictionary*)dictionary str:(NSString*)minmaxStr;
+ (NSString*)addQueryStringToUrlString:(NSString *)urlString withDictionary:(NSDictionary *)dictionary;
+ (NSDictionary*)queryComponents:(NSURL*)url;
+ (NSString *) md5:(NSString *) input;
+ (NSString*)rc4:(NSString*)str key:(NSString*)key;
+ (void)showLoading;
+ (void)hideLoading;
+ (void)recordViewedTask:(NSInteger)taskID;
+ (NSArray*)viewedTasks;

+ (BOOL)isNonEmptyText:(NSString *)text;
+ (BOOL)isValidTextInput:(NSString *)textString;
+ (BOOL)isValidEmail:(NSString *)emailString;
+ (BOOL)isValidPassword:(NSString *)passwordString;
+ (UIAlertView *)showConfirmAlertBoxWithTitle:(NSString *)title message:(NSString *)message;
+ (UIAlertView *)showConfirmAlertBoxWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate;
+ (UIAlertView *)showConfirmAlertBoxWithTitleKey:(NSString *)titleKey messageKey:(NSString *)messageKey;
+ (UIAlertView *)showConfirmAlertBoxWithTitleKey:(NSString *)titleKey messageKey:(NSString *)messageKey delegate:(id<UIAlertViewDelegate>)delegat;

+ (void)styleLoginTextField:(UITextField *)textField;

+ (NSString *)directoryPathOfAppFiles;

+(NSString *) utf8ToUnicode:(NSString *)string;

@end
