//
//  JTSpotifyPlaylistHelper.m
//  Jarre
//
//  Created by Kong king sin on 18/9/14.
//  Copyright (c) 2014 Kong king sin. All rights reserved.
//

#import "JTSpotifyPlaylistHelper.h"
#import <Spotify/Spotify.h>
@implementation JTSpotifyPlaylistHelper
+ (void)fetchAllUserPlaylistsWithSession:(SPTSession *)session callback:(void (^)(NSError *, NSArray *))callback
{
    [SPTRequest playlistsForUserInSession:session callback:^(NSError *error, id object) {
        [JTSpotifyPlaylistHelper didFetchListPageForSession:session finalCallback:callback error:error object:object allPlaylists:[NSMutableArray array]];
    }];
}

+ (void)didFetchListPageForSession:(SPTSession *)session finalCallback:(void (^)(NSError*, NSArray*))finalCallback error:(NSError *)error object:(id)object allPlaylists:(NSMutableArray *)allPlaylists
{
    if (error != nil) {
        finalCallback(error, nil);
    } else {
        if ([object isKindOfClass:[SPTPlaylistList class]] || [object isKindOfClass:[SPTListPage class]]) {
            
            SPTPlaylistList *playlistList = (SPTPlaylistList *)object;
            for (SPTPartialPlaylist *playlist in playlistList.items) {
                [allPlaylists addObject:playlist];
            }
            
            // Breakpoint set here
            if (playlistList.hasNextPage) {
                [playlistList requestNextPageWithSession:session callback:^(NSError *error, id object) {
                    [JTSpotifyPlaylistHelper didFetchListPageForSession:session
                                                           finalCallback:finalCallback
                                                                   error:error
                                                                  object:object
                                                            allPlaylists:allPlaylists];
                }];
            } else {
                finalCallback(nil, [allPlaylists copy]);
            }
        }
    }
}
@end
